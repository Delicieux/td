#!/usr/bin/python3

#-*- coding: utf-8 -*-

import cv2
import libs
import numpy as np
import theano
import lasagne
import mnist_cnn as cnn
import os

#---------------------------------------------------------IMAGERIE--------------------------------------------------


#Lire l'image puis la binariser en noir et blanc à l'aide d'un seuillage
imageRGB = cv2.imread('grille.png')
imageGrise = cv2.cvtColor(imageRGB,cv2.COLOR_BGR2GRAY)
_,imageBin = cv2.threshold(imageGrise,99,255,cv2.THRESH_BINARY)
_,contours,_= cv2.findContours(imageBin,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

#Fonction permettant de detecter les contours de la grille (qui sont desormais reperables)
def getLargestContour(cont):
    aireMax = 0
    indice_contours = 0
    for i in range(len(cont)):
        aire=cv2.contourArea(cont[i])
        if aire>aireMax:
            aireMax=aire
            indice_contours =i
    return cont[indice_contours]

#Eliminer l'aire en dehors de la grille (ce qui est en noir)
contours.remove(getLargestContour(contours))

#----------------

perim_cnt = cv2.arcLength(getLargestContour(contours), True)
cnt_sim = cv2.approxPolyDP(getLargestContour(contours),0.1*perim_cnt,True)

#Appliquer une matrice de redressement apres avoir ordonne les points correspondant aux coins
cnt_sim = libs.orderContourPoints(cnt_sim)
M=np.array([[400,0],[0,0],[0,500],[400,500]],np.float64)
M,_= cv2.findHomography(cnt_sim,M)
imageDroite = cv2.warpPerspective(imageRGB,M,(400,500))

#Blanchissement des cases puis conversion en noir et blanc avec un seuillage 
# --> imageDST est une copie de imageDroite que l'on va modifier
imageDST = np.copy(imageDroite)
for i in range(5):
    for j in range(4):
        imageDST[i*100+10:(i+1)*100-10,j*100+10:(j+1)*100-10]=(255)

imageG=cv2.cvtColor(imageDST,cv2.COLOR_BGR2GRAY)
_,imageNB=cv2.threshold(imageG,74,255,cv2.THRESH_BINARY)
_,contNB,_=cv2.findContours(imageNB,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

#Obtenir les contours des zones
imageNB = np.uint8(imageNB)
for i in range(len(contNB)):
    cv2.drawContours(imageNB,contNB,i,i+1,-1)

#Declaration des listes de recuperation des valeurs d'interet
cases = []
numZoneCases = []
cooZoneCases = []

#Recuperation des coordonnees des cases contenant un chiffre et constitution de la liste de ces cases
_,imageDroite = cv2.threshold(imageDroite,85,255,cv2.THRESH_BINARY)
for i in range(5):
    for j in range(4):
        nbzero = np.count_nonzero(imageDroite[i*100+10:(i+1)*100-10,j*100+10:(j+1)*100-10])
        if nbzero < 19200 : #seuil obtenu de maniere empirique
            cases.append(imageDroite[i*100+10:(i+1)*100-10,j*100+10:(j+1)*100-10])
            cooZoneCases += [(i,j)]
print(cooZoneCases)

#for (i,j) in cooZoneCases:
#numZoneCases.append([cnn.predict_numbers_in_cells(cases[i,j])])

#Afficher les imagettes (pas obligatoire)
for i in range(len(cases)):
    cv2.imshow('case'+str(i),cases[i])
    cv2.waitKey()


#-------------------------------------------------------------------------------------------------------------------

def predict_numbers_in_cells(cells):

    #CHECK FOR MISUSAGE
    if not os.path.isfile('model.npz'):
        print('\nERROR:: predict_numbers_in_cells() THE FILE model.npz MUST BE WITHIN THIS SCRIPT DIRECTORY !\n')
        exit()
    if not isinstance(cells, (list,)):
        print('\nERROR:: predict_numbers_in_cells() THE INPUT MUST BE A LIST OF IMAGES !\n')
        exit()

    #CONVERTING IMAGE LIST TO TENSOR
    images_tensor = np.zeros([len(cells),1,29,29],np.float64)
    for i in range(len(cells)):
        #RESIZE TO 29X29
        current_cell=np.copy(cells[i])

        #THE NUMBER MUST NOT TAKE ALL THE IMAGE SO WE FIRST RESIZE IT TO 22x22
        size_cell = 22
        current_cell=cv2.resize(current_cell,(size_cell,size_cell))

        #THEN WE INCLUDE IT IN A BIGGER IMAGE 29x29
        cell = 255*np.ones([29,29],np.uint8)
        cell[int((29-size_cell)/2):int((29-size_cell)/2)+size_cell,int((29-size_cell)/2):int((29-size_cell)/2)+size_cell] = current_cell
        current_cell = cell

        #BINARISATION
        current_cell[current_cell>100]=255
        current_cell[current_cell<255]=0
        current_cell=255-current_cell

        #NORMALIZATION
        images_tensor[i,0] = current_cell/255.0

    #BUILD ALL LAYER
    input_var = theano.tensor.tensor4('input_var','float64')
    input_layer = lasagne.layers.InputLayer(shape=(None, 1, 28, 28),input_var=input_var)
    conv1_layer = lasagne.layers.Conv2DLayer(input_layer, num_filters=32, filter_size=(5, 5))
    pool1_layer = lasagne.layers.MaxPool2DLayer(conv1_layer, pool_size=(2, 2))
    conv2_layer = lasagne.layers.Conv2DLayer(pool1_layer, num_filters=32, filter_size=(5, 5))
    pool2_layer = lasagne.layers.MaxPool2DLayer(conv2_layer, pool_size=(2, 2))
    dropout1_layer = lasagne.layers.dropout(pool2_layer, p=.5)
    dense1_layer = lasagne.layers.DenseLayer(dropout1_layer,num_units=256)
    dropout2_layer = lasagne.layers.dropout(dense1_layer, p=.5)
    output_layer = lasagne.layers.DenseLayer(dropout2_layer,num_units=10,nonlinearity=lasagne.nonlinearities.softmax)

    #LASAGNE OUTPUT WITHOUT DROPOUT
    test_prediction = lasagne.layers.get_output(output_layer, deterministic=True)

    #PREDICTION THEANO FUNCTION
    predict = theano.function([input_var],test_prediction)

    #LOAD LEARNED PARAMETERS
    #FROM mnist@LASAGNE
    with np.load('model.npz') as f:
        param_values = [f['arr_%d' % i] for i in range(len(f.files))]
    lasagne.layers.set_all_param_values(output_layer, param_values)

    #PROBA PREDICTIONS
    proba_prediction = predict(images_tensor)

    return proba_prediction

# Execution
predict_numbers_in_cells (cases)


#----------------------------------------------------------------SOLVEUR---------------------------------------------------------------------

G = [[[0,0,0,0],
      [1,1,2,2],
      [1,1,2,2],
      [3,1,4,2],
      [3,4,4,4]],
     {(0,2):1,(1,3):4,(2,1):5,(3,3):3}]

# Fonction aidant a l'etablissement des contraintes :

    # donne le nombre de cases dans la zone ou vit la case (l,c) :

def nbValeursCase(G,l,c):
    taillezone = 0;
    for i in range(0,len(G[0])):
        for j in range(0,len(G[0][0])):
            if G[0][l][c] == G[0][i][j]:
                taillezone += 1;
    return taillezone

    # donne l'ensemble des valeurs possibles pour une case (de 1 a la taille de la zone) :

def ShapesValues(G,l,c) :
    if (l,c) in G[1].keys() :
        liste = [G[1][(l,c)]];
    else :
        liste = list(range(1,nbValeursCase(G,l,c)+1))
    return liste

    # donne une liste qui contient toutes les coordonnees des cases du tableau G :

def Coordinates(G):
    liste = []
    for i in range(len(G[0])):
        for j in range(len(G[0][0])):
            liste += [(i,j)]
    return liste

    # donne une liste qui contient les coordonnees des cases voisines :

def Neighbours(G,i,j) :
    return [ (Idelta+i,Jdelta+j)
    for Idelta in (-1,0,1)
    for Jdelta in (-1,0,1)
    if 0 <= i + Idelta <= len(G[0])-1
    if 0 <= j + Jdelta <= len(G[0][0])-1
    if (i + Idelta != i) or (j + Jdelta != j)]

    # dico qui donne le nombre de case de chaque zone :

def nbValZone(G):
    d={}
    for i in range(len(G[0])) :
        for j in range(len(G[0][0])) :
            zone=G[0][i][j]
            d[zone]=1+(d[zone] if zone in d else 0)
    return d

# Nos contraintes :

    # (Cstr1) : une case doit toujours avoir une valeur :

def ValueConstraints (G) :
    return [ [ (True,i,j,k)
        for k in ShapesValues(G,i,j)
    ]
    for (i,j) in Coordinates(G)
    ]

    # (Cstr2) : la valeur dans une case est unique :

def UnicityConstraints (G) :
    return [ [ (False,i,j,k), (False,i,j,m)]
    for (i,j) in Coordinates (G)
    for k in ShapesValues(G,i,j)
    for m in ShapesValues(G,i,j)
    if k<m]

>     # (Cstr3) : une case doit avoir une valeur differente de celle de ses voisines :

def NeighboursIncompatibility (G) :
    return [ [(False,i,j,k),(False,a,b,k)]
    for (i,j) in Coordinates(G)
    for (a,b) in Neighbours(G,i,j)
    if (i,j) < (a, b)
    for k in ShapesValues(G,i,j)
    if k in ShapesValues(G,a,b)]

>     # (Cstr4) : dans une zone deux cases ne doivent pas avoir la meme valeur :

def ZoneConstraints(G):
    return [[(False,i,j,k),(False,a,b,k)]
    for (i,j) in Coordinates(G)
    for (a,b) in Coordinates(G)
    if (i,j) < (a,b)
    if G[0][i][j] == G[0][a][b]
    for k in range(1,1 + nbValZone(G)[G[0][i][j]])]

# conversion des contraintes de type tuple en des contraintes de type numerique pour minisat :

    # creation du tableau codant :

tVal = [[0] *len(G[0][0]) for i in range(len(G[0]))];
for i in range (len(G[0])):
    for j in range (len(G[0][0])):
        if (i,j) != (0,0) :
            var += nbValeursCase(G,i,j);
            tVal[i][j] = var;
        else :
            var = nbValeursCase(G,0,0);
            tVal[0][0] = var;

    #  code les tuples en des entiers relatifs :

def TupleToVar(t, tVal):
    (b, i, j, k) = t;
    assert(k in range(1, 1 + nbValeursCase(G,i,j)));
    return tVal[i][j] - k + 1 if b else -(tVal[i][j] - k + 1)

>     # insertion des contraintes de type entiers dans un fichier cnf et creation du fichier contenant la solution :

def PythonToCNF(constraints) :
    return 'p cnf {} {}\n'.format(tVal[-1][-1], len(constraints)) +
'\n'.join([' '.join([str(TupleToVar(tuple, tVal)) for tuple in constraint])
+ ' 0' for constraint in constraints])

with open("CommeTuVeux.cnf", "w") as fichier :
     fichier.write("")

with open("CommeTuVeux.cnf", "a") as fichier :
     fichier.write(PythonToCNF(ValueConstraints(G) + UnicityConstraints(G)
+ NeighboursIncompatibility(G) + ZoneConstraints(G)))

os.system("minisat CommeTuVeux.cnf soluce.cnf")

    # recuperation de la solution donnee par minisat et traduction :

with open("soluce.cnf", "r") as fichier :
    resultat = fichier.readlines()
    cnfRes = resultat[1].split()   # split met chaque mot dans une case d'un tableau

def CNFToPython(cnfRes) :
    l = [];
    suguruRes = [[0 for i in range(len(tVal[0]))] for j in
range(len(tVal))];

    for val in cnfRes :
        if '0' < val :
            l += [int(val)];
    cnfRes = l;

    for i in range(len(tVal)) :
        for j in range(len(tVal[0])) :
            for var in  cnfRes:
                if var <= tVal[i][j] :
                    suguruRes[i][j] = tVal[i][j] - var + 1;
    return suguruRes

print(CNFToPython(cnfRes));